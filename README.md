# BudgetGo

[![Build Status](https://gitlab.com/carlosmaniero/budgetgo/badges/master/build.svg)](https://gitlab.com/carlosmaniero/budgetgo/pipelines)
[![Go Report Card](https://goreportcard.com/badge/github.com/carlosmaniero/budgetgo)](https://goreportcard.com/report/github.com/carlosmaniero/budgetgo)

Simple personal budget project.
This is the REST API service. Of the platform.

# Instaling

    $ go get github.com/carlosmaniero/budgetgo
    $ cd $GOHOME/src/carlosmanieor/budgetgo
    $ go get -t -v ./...

# Running
    
    $ go main.go

