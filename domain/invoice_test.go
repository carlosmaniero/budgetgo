package domain

import (
	"testing"
	"time"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSpecInvoice(t *testing.T) {
	Convey("Scenario: Viewing a transaction", t, func() {
		Convey("Given I've a invoice", func() {
			funding := Funding{
				ID:         "bank-account",
				Name:       "Bank Account",
				Amount:     1000.00,
				ClosingDay: 10,
				PaymentDay: 10,
			}

			period := InvoicePeriod{
				Month: time.July,
				Year:  2017,
			}
			invoice := Invoice{
				Period:  &period,
				Funding: &funding,
			}

			Convey("When I add a transaction with amount equal -32.99 to this invoice", func() {
				transaction := Transaction{
					Description: "Cat food",
					Amount:      -32.99,
					Funding:     &funding,
					Date:        time.Date(period.Year, time.June, funding.ClosingDay, 0, 0, 0, 0, time.Local),
				}

				err := invoice.Add(&transaction)
				Convey("Then I can see that my current invoice is in the value of -32.99", func() {
					So(invoice.Total, ShouldEqual, -32.99)
					So(err, ShouldBeNil)
					So(invoice.Transactions[0], ShouldEqual, &transaction)
				})

				Convey("And When I Add another transaction with the amount equal 70.00", func() {
					transaction := Transaction{
						Description: "Chargeback",
						Amount:      70,
						Funding:     &funding,
						Date:        time.Date(period.Year, time.June, funding.ClosingDay, 1, 0, 0, 0, time.Local),
					}

					err := invoice.Add(&transaction)
					Convey("Then I can see that my current invoice is in the value of -32.99", func() {
						So(invoice.Total, ShouldEqual, 37.01)
						So(err, ShouldBeNil)
					})
				})
			})

			Convey("When I check for the start date", func() {
				date := invoice.GetStartDate()

				Convey("Then I can see that the start date is equal June 10, 2017", func() {
					expectedDate := time.Date(2017, time.June, 10, 0, 0, 0, 0, time.Local)
					So(date.Unix(), ShouldEqual, expectedDate.Unix())
				})
			})

			Convey("When I check for the end date", func() {
				date := invoice.GetEndDate()

				Convey("Then I can see that the end date is equal June 10, 2017", func() {
					expectedDate := time.Date(2017, time.July, 10, 0, 0, 0, 0, time.Local)
					So(date.Unix(), ShouldEqual, expectedDate.Unix())
				})
			})

			Convey("When I add a transaction out of the date range", func() {
				transaction := Transaction{
					Description: "Out of the date",
					Amount:      70,
					Funding:     &funding,
					Date:        time.Date(period.Year, period.Month, funding.ClosingDay, 1, 0, 0, 0, time.Local),
				}

				err := invoice.Add(&transaction)

				Convey("Then I can see a error", func() {
					So(err, ShouldNotBeNil)
				})

				Convey("And the total was not changed", func() {
					So(invoice.Total, ShouldEqual, 0)
				})
			})
			Convey("When I add a transaction of other funding", func() {
				otherFunding := Funding{
					ID:         "bank-account-2",
					Name:       "Bank Account",
					Amount:     1000.00,
					ClosingDay: 10,
					PaymentDay: 10,
				}
				transaction := Transaction{
					Description: "Out of the date",
					Amount:      70,
					Funding:     &otherFunding,
					Date:        time.Date(period.Year, time.June, funding.ClosingDay, 0, 0, 0, 0, time.Local),
				}

				err := invoice.Add(&transaction)

				Convey("Then I can see a error", func() {
					So(err, ShouldNotBeNil)
				})

				Convey("And the total was not changed", func() {
					So(invoice.Total, ShouldEqual, 0)
				})
			})
		})
	})
}
