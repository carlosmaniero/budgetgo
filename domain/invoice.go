package domain

import (
	"errors"
	"time"
)

// ErrTransactionOutOfTheDate is raised when the transaction does not belong
// to this invoice date interval
var ErrTransactionOutOfTheDate = errors.New("this transaction is out of the date")

// ErrInvalidTransactionFunding is raised when the transaction does not belong
// to this invoice funding
var ErrInvalidTransactionFunding = errors.New("this transaction is out of the date")

// InvoicePeriod is the period of a invoice
type InvoicePeriod struct {
	Month time.Month
	Year  int
}

// Invoice is the period representation of a funding
type Invoice struct {
	Funding      *Funding
	Transactions []*Transaction
	Period       *InvoicePeriod
	Total        float64
}

// Add a transaction to this invoice
func (invoice *Invoice) Add(transaction *Transaction) error {
	if invoice.inDateRange(transaction.Date) {
		return ErrTransactionOutOfTheDate
	}

	if invoice.Funding.ID != transaction.Funding.ID {
		return ErrInvalidTransactionFunding
	}

	invoice.Total += transaction.Amount
	invoice.Transactions = append(invoice.Transactions, transaction)
	return nil
}

// inDateRange checks if the date is inside the invoice date interval
func (invoice *Invoice) inDateRange(date time.Time) bool {
	return date.Before(invoice.GetStartDate()) || !date.Before(invoice.GetEndDate())
}

// GetStartDate return the start date of it transaction
func (invoice *Invoice) GetStartDate() time.Time {
	return time.Date(invoice.Period.Year, invoice.Period.Month-1, invoice.Funding.ClosingDay, 0, 0, 0, 0, time.Local)
}

// GetEndDate return the start date of it transaction
func (invoice *Invoice) GetEndDate() time.Time {
	return time.Date(invoice.Period.Year, invoice.Period.Month, invoice.Funding.ClosingDay, 0, 0, 0, 0, time.Local)
}
