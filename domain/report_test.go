package domain

import (
	"testing"

	. "github.com/smartystreets/goconvey/convey"
)

func TestSpecInvoicePeriodReport(t *testing.T) {
	Convey("Scenario: Generate a period report invoice", t, func() {
		Convey("Given I've tow invoices with the total of 11.11 and 33.33", func() {
			invoice1 := Invoice{
				Total: 11.11,
			}
			invoice2 := Invoice{
				Total: 33.33,
			}
			Convey("When I add the invoices to the report", func() {
				report := PeriodInvoiceReport{}
				report.Add(&invoice1)
				report.Add(&invoice2)
				Convey("Then the total of the report is 44.44", func() {
					So(report.Total, ShouldEqual, 44.44)

					Convey("And then the report have the inserted invoices", func() {
						So(report.Invoices[0], ShouldEqual, &invoice1)
						So(report.Invoices[1], ShouldEqual, &invoice2)
						So(len(report.Invoices), ShouldEqual, 2)
					})
				})
			})
		})
	})
}
