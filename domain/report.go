package domain

// PeriodInvoiceReport contains the Report use cases
type PeriodInvoiceReport struct {
	Invoices []*Invoice
	Total    float64
}

// Add a invoice to the report
// You should never change the content of the Invoices.
// Call the Add method because it calculate the total of the report
func (report *PeriodInvoiceReport) Add(invoice *Invoice) {
	report.Invoices = append(report.Invoices, invoice)
	report.Total += invoice.Total
}
