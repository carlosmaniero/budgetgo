package usecases

import (
	"testing"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSpecInvoice(t *testing.T) {
	Convey("Scenario: Viewing a invoice", t, func() {
		Convey("Given I've a valid funding with closing day equal 10", func() {
			mockTransactionRepository := transactionRepository{}
			mockFundingRepository := fundingRepository{}
			iterator := InvoiceIterator{
				TransactionRepository: &mockTransactionRepository,
				FundingRepository:     &mockFundingRepository,
			}
			funding := domain.Funding{
				ID:         "bank-account",
				ClosingDay: 10,
				PaymentDay: 10,
			}

			list := []*domain.Transaction{
				{
					Description: "First",
					Amount:      11.22,
					Funding:     &funding,
					Date:        time.Date(2017, time.Month(6), 10, 0, 0, 0, 0, time.Local),
				},
				{
					Description: "Second",
					Amount:      44.33,
					Funding:     &funding,
					Date:        time.Date(2017, time.Month(7), 9, 0, 0, 0, 0, time.Local),
				},
			}
			julyPeriod := domain.InvoicePeriod{
				Month: time.July,
				Year:  2017,
			}

			mockFundingRepository.stored = &funding
			mockTransactionRepository.transactionList = &testTransactionList{
				items: list,
			}
			Convey("When I try to get an invoice for 2017, June", func() {
				invoice, err := iterator.GetInvoiceByFundingID("bank-account", &julyPeriod)
				Convey("Then there is no error", func() {
					So(err, ShouldBeNil)
				})
				Convey("And I the invoice checks transactions between June 10, 2017 and July 09, 2017", func() {
					So(mockFundingRepository.findedID, ShouldEqual, "bank-account")
					So(mockTransactionRepository.findedFunding, ShouldEqual, &funding)

					expectedStart := time.Date(2017, time.Month(6), 10, 0, 0, 0, 0, time.Local).Unix()
					expectedEnd := time.Date(2017, time.Month(7), 10, 0, 0, 0, 0, time.Local).Unix()

					So(mockTransactionRepository.findedStart.Unix(), ShouldEqual, expectedStart)
					So(mockTransactionRepository.findedEnd.Unix(), ShouldEqual, expectedEnd)
				})
				Convey("And I can see my list of transactions and ", func() {
					So(invoice, ShouldNotBeNil)
					So(invoice.Transactions[0].Description, ShouldEqual, list[0].Description)
					So(invoice.Transactions[0].Amount, ShouldEqual, list[0].Amount)
					So(invoice.Transactions[0].Date.Unix(), ShouldEqual, list[0].Date.Unix())
					So(invoice.Transactions[1].Description, ShouldEqual, list[1].Description)
					So(invoice.Transactions[1].Amount, ShouldEqual, list[1].Amount)
					So(invoice.Transactions[1].Date.Unix(), ShouldEqual, list[1].Date.Unix())
					So(invoice.Total, ShouldEqual, 55.55)
				})
			})
		})
		Convey("Given I've a invalid funding id", func() {
			mockTransactionRepository := transactionRepository{}
			mockFundingRepository := fundingRepository{}
			iterator := InvoiceIterator{
				TransactionRepository: &mockTransactionRepository,
				FundingRepository:     &mockFundingRepository,
			}
			julyPeriod := domain.InvoicePeriod{
				Month: time.July,
				Year:  2017,
			}
			Convey("When I try to get an invoice for 2017, June", func() {
				invoice, err := iterator.GetInvoiceByFundingID("bank-account", &julyPeriod)
				Convey("Then there is an error", func() {
					So(err, ShouldNotBeNil)
					So(invoice, ShouldBeNil)
				})
			})
		})
	})
}

type testTransactionList struct {
	items   []*domain.Transaction
	current int
}

func (list *testTransactionList) Next(transaction *domain.Transaction) bool {
	if list.current >= len(list.items) {
		return false
	}
	currentTransaction := list.items[list.current]
	*transaction = *currentTransaction
	list.current++
	return true
}
