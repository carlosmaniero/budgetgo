package usecases

import (
	"errors"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
)

// FundingInteractor contains all usecases related to funding
type FundingInteractor struct {
	Repository FundingRepository
}

// Register a funding into the repository if it is valid
func (iterator *FundingInteractor) Register(funding *domain.Funding) error {
	if errs := funding.Validate(); errs != nil {
		err := ValidationErrors{errs}
		return &err
	}

	funding.ID = iterator.Repository.Store(funding)
	return nil
}

// Retrieve a funding in the repository
//
// If the funding was not found, it returns the ErrFundingNotFound error
func (iterator *FundingInteractor) Retrieve(id string) (*domain.Funding, error) {
	if funding := iterator.Repository.FindByID(id); funding != nil {
		return funding, nil
	}

	return nil, ErrFundingNotFound
}

// FundingToPay returns the list of funding that need to be paid in a period
func (iterator *FundingInteractor) FundingToPay(start time.Time, end time.Time) []*domain.Funding {
	return iterator.Repository.FindByPeriod(start, end)
}

// ErrFundingNotFound is the error returned when the funding was not found in
// the repository
var ErrFundingNotFound = errors.New("the funding was not found")

// FundingRepository interface contains the specification of an funding
// repository
type FundingRepository interface {
	Store(*domain.Funding) string
	FindByID(string) *domain.Funding
	FindByPeriod(time.Time, time.Time) []*domain.Funding
}
