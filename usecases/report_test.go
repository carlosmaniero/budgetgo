package usecases

import (
	"testing"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	. "github.com/smartystreets/goconvey/convey"
)

func TestCaseReport(t *testing.T) {
	Convey("Scenario: Generate Period report", t, func() {
		Convey("Given I've two fundings with 10 and 12 as closing day and both with 20 as payment day", func() {
			fundings := []*domain.Funding{
				{
					ID:         "funding-test-1",
					Name:       "Test funding",
					ClosingDay: 10,
					Limit:      1000,
					PaymentDay: 20,
				},
				{
					ID:         "funding-test-2",
					Name:       "Test funding",
					ClosingDay: 12,
					Limit:      1000,
					PaymentDay: 20,
				},
			}
			Convey("This fundings has two transaction each with different amount and all of June", func() {
				mockFundingRepository := fundingRepository{}
				mockTransactionRepository := mockedTransactionRepositoryForFundingReports{}

				Convey("When I ask about the report of the July month for this fundings", func() {
					iterator := ReportIteractor{
						TransactionRepository: &mockTransactionRepository,
						FundingRepository:     &mockFundingRepository,
					}

					period := domain.InvoicePeriod{
						Month: time.July,
						Year:  2017,
					}

					report := iterator.ReportByFundings(fundings, &period)

					Convey("Then I can see that the total of this report is equal of the sum of all transactions amount", func() {
						So(report.Total, ShouldEqual, 100)

						Convey("And the repository was called with correct arguments", func() {
							So(mockTransactionRepository.intervalCalls, ShouldEqual, 2)
						})
					})
				})

				Convey("When I ask about report for the interval 2017-07-20 2017-08-5", func() {
					start := time.Date(2017, 7, 20, 0, 0, 0, 0, time.Local)
					end := time.Date(2017, 7, 5, 0, 0, 0, 0, time.Local)

					mockFundingRepository.fundings = fundings

					iterator := ReportIteractor{
						TransactionRepository: &mockTransactionRepository,
						FundingRepository:     &mockFundingRepository,
					}

					report := iterator.ReportByInterval(start, end)
					Convey("Then I can see that the total of this report is equal of the sum of all transactions amount", func() {
						So(report.Total, ShouldEqual, 100)

						Convey("And the repository was called with correct arguments", func() {
							So(mockTransactionRepository.intervalCalls, ShouldEqual, 2)
						})
					})
				})
			})
		})
		Convey("Given there's no funding", func() {
			mockFundingRepository := fundingRepository{}
			mockTransactionRepository := mockedTransactionRepositoryForFundingReports{}

			Convey("When I ask about the report of the July month for this fundings", func() {
				iterator := ReportIteractor{
					TransactionRepository: &mockTransactionRepository,
					FundingRepository:     &mockFundingRepository,
				}

				period := domain.InvoicePeriod{
					Month: time.July,
					Year:  2017,
				}

				fundings := make([]*domain.Funding, 0)
				report := iterator.ReportByFundings(fundings, &period)

				Convey("Then the report is equal zero", func() {
					So(report.Total, ShouldEqual, 0)

					Convey("And the repository was not called", func() {
						So(mockTransactionRepository.intervalCalls, ShouldEqual, 0)
					})
				})
			})
		})
	})
}

type mockedTransactionRepositoryForFundingReports struct {
	intervalCalls int
}

func (m *mockedTransactionRepositoryForFundingReports) Store(*domain.Transaction) string {
	panic("not implemented")
}

func (m *mockedTransactionRepositoryForFundingReports) FindByID(string) *domain.Transaction {
	panic("not implemented")
}

func (m *mockedTransactionRepositoryForFundingReports) FindByFundingAndInterval(funding *domain.Funding, start time.Time, end time.Time) TransactionList {
	m.intervalCalls++
	list := make([]*domain.Transaction, 0)

	if funding.ID == "funding-test-1" {
		// The transaction list of the first funding

		// Check if the start and end date is the expected
		if start != time.Date(2017, 6, 10, 0, 0, 0, 0, time.Local) {
			panic("Invalid start")
		}
		if end != time.Date(2017, 7, 10, 0, 0, 0, 0, time.Local) {
			panic("Invalid end")
		}
		list = []*domain.Transaction{
			{
				Description: "First",
				Amount:      11.22,
				Funding:     funding,
				Date:        time.Date(2017, time.Month(6), 11, 0, 0, 0, 0, time.Local),
			},
			{
				Description: "Second",
				Amount:      44.33,
				Funding:     funding,
				Date:        time.Date(2017, time.Month(6), 11, 0, 0, 0, 0, time.Local),
			},
		}

	} else if funding.ID == "funding-test-2" {
		// The transaction list of the second funding

		// Check if the start and end date is the expected
		if start != time.Date(2017, 6, 12, 0, 0, 0, 0, time.Local) {
			panic("Invalid start")
		}
		if end != time.Date(2017, 7, 12, 0, 0, 0, 0, time.Local) {
			panic("Invalid end")
		}
		list = []*domain.Transaction{
			{
				Description: "Third",
				Amount:      44.0,
				Funding:     funding,
				Date:        time.Date(2017, time.Month(6), 13, 0, 0, 0, 0, time.Local),
			},
			{
				Description: "Fourth",
				Amount:      0.45,
				Funding:     funding,
				Date:        time.Date(2017, time.Month(6), 14, 0, 0, 0, 0, time.Local),
			},
		}
	} else {
		panic("It's not a valid funding.")
	}

	return &testTransactionList{
		items: list,
	}
}
