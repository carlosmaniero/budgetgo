package usecases

import (
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
)

// ReportIteractor contains some usecases of reports
type ReportIteractor struct {
	TransactionRepository TransactionRepository
	FundingRepository     FundingRepository
}

// getInvoicesByFunding receives a list of fundings, the period and a channel to report when the invoice was received.
// This method will get all of the fundings in this period and will respond in the channel the correspondent invoice
// of each funding.
func (iteractor *ReportIteractor) getInvoicesByFunding(fundings []*domain.Funding, period *domain.InvoicePeriod, invoiceChannel chan *domain.Invoice) {
	invoiceIteractor := InvoiceIterator{
		TransactionRepository: iteractor.TransactionRepository,
		FundingRepository:     iteractor.FundingRepository,
	}

	for _, funding := range fundings {
		go func(funding *domain.Funding) {
			invoice, _ := invoiceIteractor.GetInvoice(funding, period)

			if invoice != nil {
				invoiceChannel <- invoice
			}
		}(funding)
	}
}

// ReportByFundings receives a list of fundings and the period to generate the report of invoices
func (iteractor *ReportIteractor) ReportByFundings(fundings []*domain.Funding, period *domain.InvoicePeriod) *domain.PeriodInvoiceReport {
	invoiceChannel := make(chan *domain.Invoice, len(fundings))
	iteractor.getInvoicesByFunding(fundings, period, invoiceChannel)

	computed := 0
	report := domain.PeriodInvoiceReport{}
	for {
		if computed == len(fundings) {
			return &report
		}

		invoice := <-invoiceChannel
		report.Add(invoice)
		computed++
	}

}

// ReportByInterval receives the start and end date. This method will first fundings that is paid in this
// interval. It will generate the invoice of the fundings to pay and it'll generate the report.
func (iteractor *ReportIteractor) ReportByInterval(start time.Time, end time.Time) *domain.PeriodInvoiceReport {
	fundingIteractor := FundingInteractor{
		Repository: iteractor.FundingRepository,
	}
	period := domain.InvoicePeriod{
		Month: start.Month(),
		Year:  start.Year(),
	}
	fundings := fundingIteractor.FundingToPay(start, end)
	return iteractor.ReportByFundings(fundings, &period)
}
