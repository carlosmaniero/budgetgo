package usecases

import (
	"github.com/carlosmaniero/budgetgo/domain"
)

// InvoiceIterator contains the invoice use cases
type InvoiceIterator struct {
	TransactionRepository TransactionRepository
	FundingRepository     FundingRepository
}

// GetInvoice return a invoice based in a funding
func (iterator *InvoiceIterator) GetInvoice(funding *domain.Funding, period *domain.InvoicePeriod) (*domain.Invoice, error) {
	invoice := domain.Invoice{
		Funding: funding,
		Period:  period,
	}

	transactionList := iterator.TransactionRepository.FindByFundingAndInterval(funding, invoice.GetStartDate(), invoice.GetEndDate())
	transactionPointer := new(domain.Transaction)

	for transactionList.Next(transactionPointer) {
		invoice.Add(transactionPointer)
		transactionPointer = new(domain.Transaction)
	}

	return &invoice, nil
}

// GetInvoiceByFundingID returns a invoice based in a funding ID and a period.
// If any error was occurred the invoice returned will be nil
func (iterator *InvoiceIterator) GetInvoiceByFundingID(fundingID string, period *domain.InvoicePeriod) (*domain.Invoice, error) {
	fundingIterator := FundingInteractor{
		Repository: iterator.FundingRepository,
	}
	funding, err := fundingIterator.Retrieve(fundingID)

	if err != nil {
		return nil, err
	}

	return iterator.GetInvoice(funding, period)
}
