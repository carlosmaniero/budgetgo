package serializers

import (
	"encoding/json"

	"github.com/carlosmaniero/budgetgo/domain"
)

// PeriodInvoiceReportSerializer is the interface to render and get data of handlers
type PeriodInvoiceReportSerializer struct {
	Invoices []*InvoiceSerializer `json:"invoices"`
	Total    float64              `json:"total"`
}

// Loads load data from a report
func (serializer *PeriodInvoiceReportSerializer) Loads(report *domain.PeriodInvoiceReport) {
	serializer.Total = report.Total
	serializer.Invoices = make([]*InvoiceSerializer, len(report.Invoices))

	for index, invoice := range report.Invoices {
		invoiceSerializer := InvoiceSerializer{}
		invoiceSerializer.Loads(invoice)
		serializer.Invoices[index] = &invoiceSerializer
	}
}

// Serialize returns the string json representation of a report
func (serializer *PeriodInvoiceReportSerializer) Serialize() []byte {
	b, _ := json.Marshal(serializer)
	return b
}
