package serializers

import (
	"encoding/json"
	"io"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	"github.com/carlosmaniero/budgetgo/usecases"
)

// TransactionSerializer is the serializable representation of a Transaction
type TransactionSerializer struct {
	ID          string             `json:"id"`
	Description string             `json:"description"`
	Amount      float64            `json:"amount"`
	Date        time.Time          `json:"date"`
	FundingID   string             `json:"funding_id,omitempty"`
	Funding     *FundingSerializer `json:"funding"`
}

// Loads the date of a Transaction
func (data *TransactionSerializer) Loads(transaction *domain.Transaction) {
	data.ID = transaction.ID
	data.Description = transaction.Description
	data.Amount = transaction.Amount
	data.Date = transaction.Date
	data.FundingID = transaction.Funding.ID
	data.Funding = &FundingSerializer{}
	data.Funding.Loads(transaction.Funding)
}

// Serialize to json string
func (data *TransactionSerializer) Serialize() []byte {
	b, _ := json.Marshal(data)
	return b
}

// Unserialize a json string representation
//
// This return an error if the input is not an valid json representation of
// the TransactionData
func (data *TransactionSerializer) Unserialize(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(&data)
	return err
}

// TransactionListSerializer is the serializer of a transaction list
type TransactionListSerializer struct {
	TransactionList usecases.TransactionList
}

// Serialize to json string
func (serializer *TransactionListSerializer) Serialize() []byte {
	dataList := make([]TransactionSerializer, 0)
	transaction := domain.Transaction{}

	for serializer.TransactionList.Next(&transaction) {
		data := TransactionSerializer{}
		data.Loads(&transaction)
		dataList = append(dataList, data)
	}

	b, _ := json.Marshal(dataList)
	return b
}
