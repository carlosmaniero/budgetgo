package serializers

import (
	"encoding/json"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
)

// InvoiceSerializer is the serializable representation of an invoice
type InvoiceSerializer struct {
	StartPeriod  time.Time                `json:"start_period"`
	EndPeriod    time.Time                `json:"end_period"`
	Total        float64                  `json:"total"`
	Funding      *FundingSerializer       `json:"funding"`
	Transactions []*TransactionSerializer `json:"transactions"`
}

// Loads the data from a invoice to be serialized
func (serializer *InvoiceSerializer) Loads(invoice *domain.Invoice) {
	serializer.StartPeriod = invoice.GetStartDate()
	serializer.EndPeriod = invoice.GetEndDate()
	serializer.Total = invoice.Total
	serializer.Funding = &FundingSerializer{}
	serializer.Funding.Loads(invoice.Funding)

	serializer.Transactions = make([]*TransactionSerializer, len(invoice.Transactions))

	for index, value := range invoice.Transactions {
		serializer.Transactions[index] = &TransactionSerializer{}
		serializer.Transactions[index].Loads(value)
	}
}

// Serialize to json string
func (serializer *InvoiceSerializer) Serialize() []byte {
	b, _ := json.Marshal(serializer)
	return b
}
