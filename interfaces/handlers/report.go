package handlers

import (
	"net/http"
	"time"

	"github.com/carlosmaniero/budgetgo/interfaces/serializers"
	"github.com/carlosmaniero/budgetgo/usecases"
	"github.com/julienschmidt/httprouter"
)

const periodTimeFormat = "2006-01-02"

func (handlers *Handlers) GetPeriodInvoiceReport(response http.ResponseWriter, request *http.Request, params httprouter.Params) {
	defer handlers.catchPanics(response)
	startDate := params.ByName("start")
	endDate := params.ByName("end")

	start, err := time.Parse(periodTimeFormat, startDate)

	if err != nil {
		panic(&ParamError{"start date"})
	}

	end, err := time.Parse(periodTimeFormat, endDate)
	if err != nil {
		panic(&ParamError{"end date"})
	}

	iteractor := usecases.ReportIteractor{
		FundingRepository:     handlers.Application.FundingRepository,
		TransactionRepository: handlers.Application.TransactionRepository,
	}

	report := iteractor.ReportByInterval(start, end)
	serializer := serializers.PeriodInvoiceReportSerializer{}
	serializer.Loads(report)
	response.Write(serializer.Serialize())
}
