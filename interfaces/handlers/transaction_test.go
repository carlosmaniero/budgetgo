package handlers

import (
	"net/http"
	"testing"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	"github.com/carlosmaniero/budgetgo/interfaces/application"
	"github.com/carlosmaniero/budgetgo/usecases"
	"github.com/julienschmidt/httprouter"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSpecTransaction(t *testing.T) {
	Convey("Scenario: Registering an transaction", t, func() {
		app := application.New()
		handlers := Handlers{Application: app}
		transactionResponse := HandlerResponseMock{}

		fundingIteractor := usecases.FundingInteractor{Repository: app.FundingRepository}
		funding := domain.Funding{
			Name:       "Beer account",
			Amount:     1,
			ClosingDay: 2,
			PaymentDay: 2,
			Limit:      3,
		}

		if err := fundingIteractor.Register(&funding); err != nil {
			panic(err)
		}

		Convey("Given I've a valid transaction json representation", func() {
			now := time.Now().Format(time.RFC3339Nano)

			request := http.Request{
				Body: NewRequestBodyMock("{\"description\": \"8 beers\", \"funding_id\": \"" + funding.ID + "\", \"amount\": 10, \"date\": \"" + now + "\"}"),
			}

			Convey("When I sent it to the handler", func() {
				handlers.TransactionCreate(&transactionResponse, &request, nil)

				Convey("Then the transaction was created successly", func() {
					So(transactionResponse.ResponseBody, ShouldEqual, "{\"id\":\"1\",\"description\":\"8 beers\",\"amount\":10,\"date\":\""+now+"\",\"funding_id\":\"1\",\"funding\":{\"id\":\"1\",\"name\":\"Beer account\",\"limit\":3,\"amount\":1,\"closing_day\":2,\"payment_day\":2}}")
					So(transactionResponse.StatusCode, ShouldEqual, 201)
				})
			})
		})

		Convey("Given I've a invalid transaction json representation", func() {
			request := http.Request{
				Body: NewRequestBodyMock("{}"),
			}

			Convey("When I sent it to the handler", func() {
				handlers.TransactionCreate(&transactionResponse, &request, nil)

				Convey("Then the transaction was not created successly", func() {
					So(transactionResponse.ResponseBody, ShouldContainSubstring, "the funding was not found")
					So(transactionResponse.StatusCode, ShouldEqual, 400)
				})
			})
		})
	})

	Convey("Scenario: Retrieving a transaction", t, func() {
		app := application.New()
		handlers := Handlers{Application: app}

		Convey("Given I've a created transaction", func() {
			fundingIterator := usecases.FundingInteractor{Repository: app.FundingRepository}
			funding := domain.Funding{
				Name:       "Pet account",
				Amount:     1,
				ClosingDay: 2,
				PaymentDay: 2,
				Limit:      3,
			}

			if err := fundingIterator.Register(&funding); err != nil {
				panic(err)
			}

			iteractor := usecases.TransactionInteractor{Repository: app.TransactionRepository}
			transaction := domain.Transaction{
				Description: "Golpher Shampoo",
				Amount:      25.50,
				Date:        time.Now(),
				Funding:     &funding,
			}

			if err := iteractor.Register(&transaction); err != nil {
				panic(err)
			}

			Convey("When I get it from the transaction entrypoint", func() {
				transactionResponse := HandlerResponseMock{}
				request := http.Request{}
				params := make(httprouter.Params, 0)
				params = append(params, httprouter.Param{Key: "id", Value: transaction.ID})
				handlers.TransactionRetrieve(&transactionResponse, &request, params)

				Convey("Then the transaction is returned", func() {
					So(transactionResponse.ResponseBody, ShouldContainSubstring, "{\"id\":\"1\",\"description\":\"Golpher Shampoo\",\"amount\":25.5,\"date\":\""+transaction.Date.Format(time.RFC3339Nano)+"\",\"funding_id\":\"1\",\"funding\":{\"id\":\"1\",\"name\":\"Pet account\",\"limit\":3,\"amount\":1,\"closing_day\":2,\"payment_day\":2}}")
				})
			})

			Convey("Given I've a list of transactions", func() {
				app := application.New()
				handlers := Handlers{Application: app}
				funding := domain.Funding{
					Name:       "Beer account",
					Amount:     1,
					ClosingDay: 2,
					PaymentDay: 2,
					Limit:      3,
				}

				fundingIterator := usecases.FundingInteractor{
					Repository: app.FundingRepository,
				}

				transactionIterator := usecases.TransactionInteractor{
					Repository: app.TransactionRepository,
				}

				if err := fundingIterator.Register(&funding); err != nil {
					panic(err)
				}

				initialDate := time.Date(2017, time.June, 29, 0, 0, 0, 0, time.Local)
				for day := 0; day < 3; day++ {
					transaction := domain.Transaction{
						Description: "My transaction",
						Amount:      10,
						Date:        time.Now(),
						Funding:     &funding,
					}
					transaction.Date = initialDate.AddDate(0, 0, day)
					transactionIterator.Register(&transaction)
				}

				Convey("When I get the list of transaction in a month", func() {
					request := http.Request{}
					params := make(httprouter.Params, 0)
					transactionResponse := HandlerResponseMock{}

					params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
					params = append(params, httprouter.Param{Key: "month", Value: "6"})
					params = append(params, httprouter.Param{Key: "year", Value: "2017"})

					handlers.TransactionRetrieveByFundingMonth(&transactionResponse, &request, params)

					Convey("Then I can see the list of transactions", func() {
						So(transactionResponse.ResponseBody, ShouldContainSubstring, "{\"id\":\"1\"")
						So(transactionResponse.ResponseBody, ShouldContainSubstring, "{\"id\":\"2\"")
						So(transactionResponse.ResponseBody, ShouldNotContainSubstring, "{\"id\":\"3\"")
						So(transactionResponse.StatusCode, ShouldEqual, 200)
					})
				})

				Convey("When I get a list with a invalid fundingID", func() {
					Convey("Then I can see a error", func() {
						request := http.Request{}
						params := make(httprouter.Params, 0)
						transactionResponse := HandlerResponseMock{}

						params = append(params, httprouter.Param{Key: "fundingID", Value: "funding-not-found"})
						params = append(params, httprouter.Param{Key: "month", Value: "6"})
						params = append(params, httprouter.Param{Key: "year", Value: "2017"})

						handlers.TransactionRetrieveByFundingMonth(&transactionResponse, &request, params)

						Convey("Then I can see the list of transactions", func() {
							So(transactionResponse.ResponseBody, ShouldContainSubstring, "the funding was not found")
							So(transactionResponse.StatusCode, ShouldEqual, 400)
						})
					})
				})

				Convey("When I get a list with a invalid month", func() {
					Convey("Then I can see a error", func() {
						request := http.Request{}
						params := make(httprouter.Params, 0)
						transactionResponse := HandlerResponseMock{}

						params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
						params = append(params, httprouter.Param{Key: "month", Value: "0"})
						params = append(params, httprouter.Param{Key: "year", Value: "2017"})

						handlers.TransactionRetrieveByFundingMonth(&transactionResponse, &request, params)

						Convey("Then I can see the list of transactions", func() {
							So(transactionResponse.ResponseBody, ShouldContainSubstring, "this is a invalid month")
							So(transactionResponse.StatusCode, ShouldEqual, 400)
						})
					})
				})
				Convey("When I get a list with a non numeric month", func() {
					Convey("Then I can see a error", func() {
						request := http.Request{}
						params := make(httprouter.Params, 0)
						transactionResponse := HandlerResponseMock{}

						params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
						params = append(params, httprouter.Param{Key: "month", Value: "NaN"})
						params = append(params, httprouter.Param{Key: "year", Value: "2017"})

						handlers.TransactionRetrieveByFundingMonth(&transactionResponse, &request, params)

						Convey("Then I can see the list of transactions", func() {
							So(transactionResponse.ResponseBody, ShouldContainSubstring, `the \"month\" url param is invalid`)
						})
					})
				})
				Convey("When I get a list with a non numeric year", func() {
					Convey("Then I can see a error", func() {
						request := http.Request{}
						params := make(httprouter.Params, 0)
						transactionResponse := HandlerResponseMock{}

						params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
						params = append(params, httprouter.Param{Key: "month", Value: "5"})
						params = append(params, httprouter.Param{Key: "year", Value: "golpher-year"})

						handlers.TransactionRetrieveByFundingMonth(&transactionResponse, &request, params)

						Convey("Then I can see the list of transactions", func() {
							So(transactionResponse.ResponseBody, ShouldContainSubstring, `the \"year\" url param is invalid`)
						})
					})
				})
			})

			Convey("Given I've a uncreated transaction", func() {
				Convey("When I get it from the transaction entrypoint", func() {
					transactionResponse := HandlerResponseMock{}
					request := http.Request{}
					params := make(httprouter.Params, 0)
					params = append(params, httprouter.Param{Key: "id", Value: "666"})
					handlers.TransactionRetrieve(&transactionResponse, &request, params)

					Convey("Then I can see that the transaction does not exists", func() {
						So(transactionResponse.ResponseBody, ShouldContainSubstring, "{\"type\":\"not-found\",\"message\":\"the transaction was not found\"}")
						So(transactionResponse.StatusCode, ShouldEqual, 404)
					})
				})
			})
		})
	})
}
