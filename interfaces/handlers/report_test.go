package handlers

import (
	"net/http"
	"testing"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	"github.com/carlosmaniero/budgetgo/interfaces/application"
	"github.com/carlosmaniero/budgetgo/usecases"
	"github.com/julienschmidt/httprouter"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSpecReport(t *testing.T) {
	Convey("Scenario: Consulting a report", t, func() {
		Convey("Given I've no funding", func() {
			app := application.New()
			handlers := Handlers{Application: app}

			Convey("When I ask for a report for 2017, July.", func() {
				params := make(httprouter.Params, 0)
				params = append(params, httprouter.Param{Key: "start", Value: "2017-07-01"})
				params = append(params, httprouter.Param{Key: "end", Value: "2017-07-31"})

				response := HandlerResponseMock{}
				request := http.Request{}

				handlers.GetPeriodInvoiceReport(&response, &request, params)
				Convey("Then I can see an empty report with zero as total", func() {
					So(response.ResponseBody, ShouldEqual, `{"invoices":[],"total":0}`)
				})
			})
		})
		Convey("Given I've a funding and transactions to June month", func() {
			app := application.New()
			handlers := Handlers{Application: app}

			funding := domain.Funding{
				Name:       "Bank accound",
				Amount:     1050.99,
				Limit:      100,
				ClosingDay: 15,
				PaymentDay: 20,
			}

			fundingIterator := usecases.FundingInteractor{
				Repository: app.FundingRepository,
			}

			fundingIterator.Register(&funding)

			transaction := domain.Transaction{
				Description: "Cat Food",
				Amount:      150,
				Date:        time.Date(2017, 6, 14, 0, 0, 0, 0, time.UTC),
				Funding:     &funding,
			}

			transactionIterator := usecases.TransactionInteractor{
				Repository: app.TransactionRepository,
			}

			transactionIterator.Register(&transaction)

			Convey("When I ask for a report for 2017, June.", func() {
				params := make(httprouter.Params, 0)
				params = append(params, httprouter.Param{Key: "start", Value: "2017-06-15"})
				params = append(params, httprouter.Param{Key: "end", Value: "2017-07-01"})

				response := HandlerResponseMock{}
				request := http.Request{}

				handlers.GetPeriodInvoiceReport(&response, &request, params)
				Convey("Then I can see an empty report with zero as total", func() {
					So(response.ResponseBody, ShouldContainSubstring, `"total":150`)
				})
			})
			Convey("When I ask for a report for 2017, September.", func() {
				params := make(httprouter.Params, 0)
				params = append(params, httprouter.Param{Key: "start", Value: "2017-09-01"})
				params = append(params, httprouter.Param{Key: "end", Value: "2017-09-30"})

				response := HandlerResponseMock{}
				request := http.Request{}

				handlers.GetPeriodInvoiceReport(&response, &request, params)
				Convey("Then I can see an empty report with zero as total", func() {
					So(response.ResponseBody, ShouldContainSubstring, `total":0`)
				})
			})
		})
	})
}
