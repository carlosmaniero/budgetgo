package handlers

import "github.com/carlosmaniero/budgetgo/interfaces/application"

// Handlers contains all entrypoints handlers of the application
type Handlers struct {
	Application *application.Application
}
