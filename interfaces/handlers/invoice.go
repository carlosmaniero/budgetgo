package handlers

import (
	"net/http"
	"strconv"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	"github.com/carlosmaniero/budgetgo/interfaces/serializers"
	"github.com/carlosmaniero/budgetgo/usecases"
	"github.com/julienschmidt/httprouter"
)

// GetInvoice returns a invoice. This method need to receive three params:
// A month, a year and a fundingID to generate the invoice
func (handlers *Handlers) GetInvoice(response http.ResponseWriter, request *http.Request, params httprouter.Params) {
	defer handlers.catchPanics(response)
	iterator := usecases.InvoiceIterator{
		TransactionRepository: handlers.Application.TransactionRepository,
		FundingRepository:     handlers.Application.FundingRepository,
	}

	month, err := strconv.Atoi(params.ByName("month"))

	if err != nil || month > 12 || month < 1 {
		handlers.paramErrorHandler(&ParamError{"month"}, response)
		return
	}

	year, err := strconv.Atoi(params.ByName("year"))
	if err != nil {
		handlers.paramErrorHandler(&ParamError{"year"}, response)
		return
	}

	fundingID := params.ByName("fundingID")
	period := domain.InvoicePeriod{
		Month: time.Month(month),
		Year:  year,
	}

	invoice, err := iterator.GetInvoiceByFundingID(fundingID, &period)

	if err != nil {
		handlers.usecaseErrorHandler(err, response)
		return
	}

	serializer := serializers.InvoiceSerializer{}
	serializer.Loads(invoice)

	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(http.StatusOK)
	response.Write(serializer.Serialize())
}
