package handlers

import (
	"net/http"
	"testing"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	"github.com/carlosmaniero/budgetgo/interfaces/application"
	"github.com/carlosmaniero/budgetgo/usecases"
	"github.com/julienschmidt/httprouter"
	. "github.com/smartystreets/goconvey/convey"
)

func TestSpecInvoice(t *testing.T) {
	Convey("Scenario: Retrieving a transaction", t, func() {
		Convey("Given I've a list of transactions", func() {
			app := application.New()
			handlers := Handlers{Application: app}
			funding := domain.Funding{
				Name:       "Beer account",
				Amount:     1,
				ClosingDay: 2,
				PaymentDay: 2,
				Limit:      3,
			}

			fundingIterator := usecases.FundingInteractor{
				Repository: app.FundingRepository,
			}

			transactionIterator := usecases.TransactionInteractor{
				Repository: app.TransactionRepository,
			}

			if err := fundingIterator.Register(&funding); err != nil {
				panic(err)
			}

			initialDate := time.Date(2017, time.June, 30, 0, 0, 0, 0, time.Local)
			for day := 0; day < 3; day++ {
				transaction := domain.Transaction{
					Description: "My transaction",
					Amount:      10.75 + float64(day+1),
					Date:        time.Now(),
					Funding:     &funding,
				}
				transaction.Date = initialDate.AddDate(0, 0, day)
				transactionIterator.Register(&transaction)
			}

			Convey("When I get the list of transaction in a month", func() {
				request := http.Request{}
				params := make(httprouter.Params, 0)
				transactionResponse := HandlerResponseMock{}

				params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
				params = append(params, httprouter.Param{Key: "month", Value: "7"})
				params = append(params, httprouter.Param{Key: "year", Value: "2017"})

				handlers.GetInvoice(&transactionResponse, &request, params)

				Convey("Then I can see the list of transactions", func() {
					So(transactionResponse.ResponseBody, ShouldContainSubstring, "{\"id\":\"1\"")
					So(transactionResponse.ResponseBody, ShouldContainSubstring, "{\"id\":\"2\"")
					So(transactionResponse.ResponseBody, ShouldNotContainSubstring, "{\"id\":\"3\"")
					So(transactionResponse.ResponseBody, ShouldContainSubstring, "\"total\":24.5,")
					So(transactionResponse.StatusCode, ShouldEqual, 200)
				})
			})

			Convey("When I get a list with a invalid fundingID", func() {
				Convey("Then I can see a error", func() {
					request := http.Request{}
					params := make(httprouter.Params, 0)
					transactionResponse := HandlerResponseMock{}

					params = append(params, httprouter.Param{Key: "fundingID", Value: "funding-not-found"})
					params = append(params, httprouter.Param{Key: "month", Value: "7"})
					params = append(params, httprouter.Param{Key: "year", Value: "2017"})

					handlers.GetInvoice(&transactionResponse, &request, params)

					Convey("Then I can see the error message", func() {
						So(transactionResponse.ResponseBody, ShouldContainSubstring, "the funding was not found")
					})
				})
			})

			Convey("When I get a list with a invalid month", func() {
				Convey("Then I can see a error", func() {
					request := http.Request{}
					params := make(httprouter.Params, 0)
					transactionResponse := HandlerResponseMock{}

					params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
					params = append(params, httprouter.Param{Key: "month", Value: "0"})
					params = append(params, httprouter.Param{Key: "year", Value: "2017"})

					handlers.GetInvoice(&transactionResponse, &request, params)

					Convey("Then I can see a message error", func() {
						So(transactionResponse.ResponseBody, ShouldContainSubstring, "the \\\"month\\\" url param is invalid")
						So(transactionResponse.StatusCode, ShouldEqual, 400)
					})
				})
			})
			Convey("When I get a list with a non numeric month", func() {
				Convey("Then I can see a error", func() {
					request := http.Request{}
					params := make(httprouter.Params, 0)
					transactionResponse := HandlerResponseMock{}

					params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
					params = append(params, httprouter.Param{Key: "month", Value: "NaN"})
					params = append(params, httprouter.Param{Key: "year", Value: "2017"})

					handlers.GetInvoice(&transactionResponse, &request, params)

					Convey("Then I can see the list of transactions", func() {
						So(transactionResponse.ResponseBody, ShouldContainSubstring, `the \"month\" url param is invalid`)
					})
				})
			})
			Convey("When I get a list with a non numeric year", func() {
				Convey("Then I can see a error", func() {
					request := http.Request{}
					params := make(httprouter.Params, 0)
					transactionResponse := HandlerResponseMock{}

					params = append(params, httprouter.Param{Key: "fundingID", Value: funding.ID})
					params = append(params, httprouter.Param{Key: "month", Value: "5"})
					params = append(params, httprouter.Param{Key: "year", Value: "golpher-year"})

					handlers.GetInvoice(&transactionResponse, &request, params)

					Convey("Then I can see the list of transactions", func() {
						So(transactionResponse.ResponseBody, ShouldContainSubstring, `the \"year\" url param is invalid`)
						So(transactionResponse.StatusCode, ShouldEqual, 400)
					})
				})
			})
		})
	})
}
