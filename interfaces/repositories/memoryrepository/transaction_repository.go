package memoryrepository

import (
	"sort"
	"strconv"
	"sync"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	"github.com/carlosmaniero/budgetgo/usecases"
)

// MemoryTransactionRepository implements the usecases.TransactionRepository
//
// This struct will use the memory to store and retrieve all Transactions
type MemoryTransactionRepository struct {
	transactions []*domain.Transaction
	mux          sync.Mutex
}

// Store a Transaction
//
// This mathod raises an panic if the number of 5 transactions is exceeded
func (repository *MemoryTransactionRepository) Store(transaction *domain.Transaction) string {
	repository.mux.Lock()
	repository.transactions = append(repository.transactions, transaction)
	id := strconv.Itoa(len(repository.transactions))
	repository.mux.Unlock()
	return id
}

// FindByID returns an transaction by ID
func (repository *MemoryTransactionRepository) FindByID(id string) *domain.Transaction {
	index, err := strconv.Atoi(id)

	if err != nil || len(repository.transactions) < index {
		return nil
	}

	return repository.transactions[index-1]
}

// FindByFundingAndInterval find transactions by funding in a determined interval
func (repository *MemoryTransactionRepository) FindByFundingAndInterval(funding *domain.Funding, start time.Time, end time.Time) usecases.TransactionList {
	sort.Sort(transactionDateSorter(repository.transactions))

	return &transactionList{
		start:        start,
		end:          end,
		funding:      funding,
		transactions: repository.transactions,
	}
}

type transactionList struct {
	start        time.Time
	end          time.Time
	funding      *domain.Funding
	transactions []*domain.Transaction
	lastItem     int
}

func (list *transactionList) Next(transaction *domain.Transaction) bool {
	for list.lastItem < len(list.transactions) {
		currentTransaction := list.transactions[list.lastItem]
		list.lastItem++

		if !currentTransaction.Date.Before(list.end) {
			return false
		}

		if currentTransaction.Funding.ID == list.funding.ID {
			if !currentTransaction.Date.Before(list.start) {
				*transaction = *currentTransaction
				return true
			}
		}
	}

	return false
}

type transactionDateSorter []*domain.Transaction

func (t transactionDateSorter) Len() int {
	return len(t)
}

func (t transactionDateSorter) Less(i int, j int) bool {
	return t[i].Date.Before(t[j].Date)
}

func (t transactionDateSorter) Swap(i int, j int) {
	t[i], t[j] = t[j], t[i]
}

// NewMemoryTransactionRepository Create a new transaction memory repository
func NewMemoryTransactionRepository() usecases.TransactionRepository {
	return &MemoryTransactionRepository{transactions: make([]*domain.Transaction, 0)}
}
