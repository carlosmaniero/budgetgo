package memoryrepository

import (
	"strconv"
	"time"

	"github.com/carlosmaniero/budgetgo/domain"
	"github.com/carlosmaniero/budgetgo/usecases"
)

// MemoryFundingRepository implements the usecases.FundingRepository
//
// This struct will use the memory to store and retrieve all fundings
type MemoryFundingRepository struct {
	fundings []*domain.Funding
}

// Store a Funding
func (repository *MemoryFundingRepository) Store(funding *domain.Funding) string {
	repository.fundings = append(repository.fundings, funding)
	return strconv.Itoa(len(repository.fundings))
}

// FindByPeriod returns funding in a specific period
func (repository *MemoryFundingRepository) FindByPeriod(start time.Time, end time.Time) []*domain.Funding {
	fundings := make([]*domain.Funding, 0)

	for _, funding := range repository.fundings {
		fundingDate := start.AddDate(0, 0, funding.ClosingDay-start.Day())
		if !fundingDate.Before(start) && fundingDate.Before(end) {
			fundings = append(fundings, funding)
		}
	}

	return fundings
}

// FindByID a Funding. It will return nil if the Funding was not founded
func (repository *MemoryFundingRepository) FindByID(id string) *domain.Funding {
	index, err := strconv.Atoi(id)

	if err != nil || len(repository.fundings) < index {
		return nil
	}

	return repository.fundings[index-1]
}

// NewMemoryFundingRepository Create a new funding memory repository
func NewMemoryFundingRepository() usecases.FundingRepository {
	return &MemoryFundingRepository{make([]*domain.Funding, 0)}
}
